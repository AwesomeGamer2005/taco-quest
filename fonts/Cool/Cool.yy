{
    "id": "4428345c-806d-4d66-942e-07ec643f8bf4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "Cool",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Absolute",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6b39ce0b-7b9f-46a1-b6cb-12e6f13ec493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 80,
                "y": 90
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cf82032d-b88a-46ed-9b8d-304ae4343a23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 56,
                "y": 90
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8c6579e8-357e-41a9-95ec-5ce4e327ed3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 82,
                "y": 68
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a204327d-3b43-47d0-ad27-412a4ac95f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 152,
                "y": 24
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "bbe9100d-e747-431d-959f-21220575d57f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "80c1502d-2e46-4e09-9fef-55f46bda218e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 138,
                "y": 24
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "40953337-3ece-42ed-94fa-2947ea03fb20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 166,
                "y": 24
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6fdab0d0-0e97-4fce-a11d-5ee58ef9883a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 3,
                "shift": 4,
                "w": 3,
                "x": 96,
                "y": 90
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "341f8fcd-f106-424e-ab69-c84c5a8ceda5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 32,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e87534f1-79f3-4325-8c38-0fd58472a050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 234,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c9084f51-2fde-4b8e-8865-0da6f1dec934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "12f719bd-fbfa-4a20-be97-6a6768b8e716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 90
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "33266167-9003-4da2-93c4-544d7d716e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 90
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a8743524-2cf0-46f8-aed6-0d7a8492e2aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 246,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9b6f90a7-d72e-48d6-96d4-821b21fe3051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 91,
                "y": 90
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "aa39f6a4-4336-41c8-b220-f3182105238a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 197,
                "y": 68
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c06f9b8c-d32f-4725-a0e3-cc8914b9e840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 72,
                "y": 68
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8394913e-84c9-4716-aec8-ed046f7a1b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 90
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "82e48642-c5ca-49c3-8dfa-564c4677c968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 92,
                "y": 68
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "30817e22-4a3d-463d-9a1f-4d62d019505e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 102,
                "y": 68
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "fd27d0e8-c147-4d22-b7d8-3eb374593241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 213,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8c34d89d-c760-4378-8755-40dc0cca34ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "71dd8858-ff3f-4723-a4b3-4d421ec4e5c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 68
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a0428ade-c01f-4b9d-bf30-b339efec3e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8c303818-c9b9-4f5b-b799-ae118ab8f381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 191,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "67cc769f-bf66-45fc-b6ab-477b2e5b5f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 112,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0ec8d6c0-f430-459c-bf19-a7062420716a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 90
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4b8cd26e-1467-49a4-a3dd-fb2c91722f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 62,
                "y": 90
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ee5cd932-9b74-4005-a5c1-efd8d1419a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "608f4e30-4746-4c89-808e-69c773426c6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 222,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "458ba1d0-1923-4ce7-a629-99711b56f77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0e63e495-a6f5-4b51-a5ff-2bd5c4f0612a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "50ac9652-dc4f-478f-9b9c-4f2c6dcd2e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 82,
                "y": 24
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2680da45-872d-4ece-8c95-4533bd55610f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b6377d10-db11-40be-b88a-155cfb159400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 15,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "11635ad7-4006-40bc-868f-f1ea33e817d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 10,
                "x": 232,
                "y": 24
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d532d284-a25e-44c9-8ceb-e561b5a4c114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 24
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d087cb0f-cc6b-4bd4-bd28-624eb238977b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 16,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "adf86c6d-acf5-43ba-a0c3-ff902c7e3545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 16,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f66e2025-48fb-4dd7-9230-f5f3f57eb4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "562e4d05-c678-42f1-b92c-f7196ed52076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 124,
                "y": 24
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ee5c9b73-f2b8-472d-9e46-8c54b2a61db1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 10,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "65b2198d-e7d9-4a56-a428-e1926c7dd98d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 14,
                "x": 18,
                "y": 24
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c3f79e70-9660-48c7-98a4-a68033adf674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6461c8b0-9f41-4bd3-ae69-ab26704eb882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 18,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "39f115d1-19ab-4ce4-bf53-a8d93719de91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 110,
                "y": 24
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "83663db1-12da-4da1-afb5-7605436f6edf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4362a758-e828-4de6-a13f-e3694180386f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 180,
                "y": 46
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "29d6841e-2c78-417e-9ff2-2c7de66defc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d14d0c41-6819-4bb5-af59-892f3e2d10e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 74,
                "y": 46
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "33a40bfa-8aee-4465-8d78-7074704f3bf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e2b88108-e425-45cf-8342-7f1b04b88d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 16,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "beb223ee-fa88-4498-9639-148b00d29489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 16,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3aa4c858-96c0-4cd6-80c0-4c9fd8ba1b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 193,
                "y": 24
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "767a79a8-dbec-424f-869e-bb5287a34e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 98,
                "y": 46
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b58fad11-3893-4637-aa0d-8736511251da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 34,
                "y": 24
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bf7bbcdf-badf-424d-851e-f5f4ec68a613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 15,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "97f37d33-02b5-4054-b8fc-238f9a30205b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": -7,
                "shift": 9,
                "w": 18,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5c5f0a9a-dfb2-43ea-95bb-006410284869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 110,
                "y": 46
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "dfdfbaf8-4e21-4420-a674-08ded2118527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 4,
                "shift": 12,
                "w": 6,
                "x": 10,
                "y": 90
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "08fb9559-b290-40c5-9f66-81de45e88f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 42,
                "y": 68
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1b1815f1-ec81-474b-b5e2-edec95923789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 2,
                "y": 90
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8c02445e-2af9-4949-a889-523d4672e060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 46
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b9be0a43-1a39-4145-b8e1-3e72241289c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b0db59de-7057-4352-92f7-3f609c0003e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 68,
                "y": 90
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "31717c3e-8ec7-44a2-893b-ff43e48471fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 22,
                "y": 68
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2dd3bedd-8795-4734-887e-96787893bf58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": -1,
                "shift": 5,
                "w": 8,
                "x": 224,
                "y": 46
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7813aca3-fc6e-42f1-a44f-d9535ff18116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 238,
                "y": 68
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "812f944d-bef4-4770-89f0-ca12c65c913a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 179,
                "y": 68
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1ce8e0af-f459-4389-8228-22ca1cdbaec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 230,
                "y": 68
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0019c940-7e8c-449b-82c9-02fdfc473499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": -1,
                "shift": 3,
                "w": 6,
                "x": 214,
                "y": 68
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3e2da6fd-6207-487d-b01e-2b07952470b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": -5,
                "shift": 6,
                "w": 12,
                "x": 96,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7cc4a2af-895e-44b2-9c6d-bb6c5867ff7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 52,
                "y": 68
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0742a1ac-4423-4d65-a756-54370fb41120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 42,
                "y": 90
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1ff73efa-74ec-4238-b9d7-4149dad8873a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": -7,
                "shift": 3,
                "w": 11,
                "x": 180,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "97ffbc42-f51e-4323-8e75-5ec5eaa75d25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 142,
                "y": 68
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2ba226e2-fcba-4b4c-9cb2-b7d9b330cd46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 49,
                "y": 90
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "366da128-9ad4-4c02-8421-05cffb2c0411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 11,
                "x": 206,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "403429f8-ca6c-4677-be11-2afa5a2cdcb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 244,
                "y": 46
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "154b0ce6-cf9e-48d8-859c-65bdfcbcbb26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": -1,
                "shift": 5,
                "w": 8,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0b3272ce-6850-40c7-9dbf-d7fc4b26f4fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": -2,
                "shift": 5,
                "w": 9,
                "x": 169,
                "y": 46
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6b340304-ba49-4ae0-a2a4-6a88f8b3f836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 161,
                "y": 68
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "31f58fd1-65a9-4e48-802f-63b4f9cadac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 206,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7795b81e-061e-4fe4-a1f8-43637d139887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": -2,
                "shift": 5,
                "w": 8,
                "x": 62,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "18f1859f-c084-4dd3-91c9-3c140020c574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": -2,
                "shift": 3,
                "w": 14,
                "x": 66,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "36dd4f55-9a13-4bc1-8706-6cba1b80c975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 12,
                "y": 68
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c643b135-52a8-4ecb-8249-62390ef8d5f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 34,
                "y": 90
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "17da69de-1019-4fed-8428-84fd3bc67a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 158,
                "y": 46
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c5e41dd8-f135-4f67-954c-ba06e9f6a485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": -2,
                "shift": 5,
                "w": 7,
                "x": 188,
                "y": 68
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7714afda-35c5-46bd-afd7-3fbdf4003858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": -4,
                "shift": 6,
                "w": 11,
                "x": 219,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "edea60dd-7f60-4a63-ae71-78594e58fea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 152,
                "y": 68
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "64963aba-ffc7-427e-bba3-2d869e51fb82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 122,
                "y": 68
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0d99b8df-b011-4d4a-92b2-09585f7b6b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 5,
                "shift": 12,
                "w": 2,
                "x": 101,
                "y": 90
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "778ceebf-4e24-471a-8111-c1c17c3ea2a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 132,
                "y": 68
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "cbfd5c4d-d661-4854-81cc-fe6487f5392c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 46
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "291d1634-f96b-4fa8-be02-bcdbd3bf0153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 49
        },
        {
            "id": "8d12240e-d4e0-4944-a116-c3efd09679c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 52
        },
        {
            "id": "e1de2c39-451c-4365-a2da-f8f4ae889d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 49
        },
        {
            "id": "3156454f-a59e-4814-9294-188962817e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 48
        },
        {
            "id": "ef442765-b294-40a3-9b27-2da4233d0ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 49
        },
        {
            "id": "33fadaed-c842-4595-952d-e4545a6de71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 50
        },
        {
            "id": "944bb8f7-a63b-4457-9d30-cde7260f338e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 51
        },
        {
            "id": "7c781ef8-c247-4f17-ac76-b0508e356003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 52
        },
        {
            "id": "aca6e1ca-9f08-4e62-b99a-1fa52d00e83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 53
        },
        {
            "id": "ee0f677b-f66e-4693-ba4b-e21f89039421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 54
        },
        {
            "id": "0005e0c0-8c7d-4f49-b699-a950143a1507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 55
        },
        {
            "id": "60acf902-7102-42c0-928d-3110b3b26e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 56
        },
        {
            "id": "49975f1a-04d0-40fd-9e61-9a85816ba315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 57
        },
        {
            "id": "8d5a15a8-845c-45c3-b561-31e410b07dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 48
        },
        {
            "id": "c72f10ba-a42c-4807-93e6-a0c970a03ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 50
        },
        {
            "id": "5e73d028-b512-4b1d-bd3c-ab0249330bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 51
        },
        {
            "id": "44be1d97-3ced-42e3-8f00-84cca69599bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 52
        },
        {
            "id": "afc13b44-8840-4c3b-9f5a-7f7327894fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 54
        },
        {
            "id": "4fe70a60-580c-47be-a724-b87ee6344f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 56
        },
        {
            "id": "63428b39-3943-4258-abbc-20bd5119e018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 57
        },
        {
            "id": "5d7f2385-2dad-433e-a421-03c9f539b74f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 50
        },
        {
            "id": "a7863c7d-ce29-43d4-9221-8ba8edcb366e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 54
        },
        {
            "id": "a0ec6f22-33da-411e-8797-21c32327a90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 56
        },
        {
            "id": "9cfd7848-a4a6-4145-96a9-8364173f761e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 57
        },
        {
            "id": "462fe866-9f32-40b9-aefb-dd52e90b1808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 56
        },
        {
            "id": "6baee1bd-a02a-433e-923c-89cd647fa6bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 97
        },
        {
            "id": "bdca7da8-03af-4a5c-914c-f9a9fd1f956c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 101
        },
        {
            "id": "857677a7-b98d-42be-a86d-b3557508617d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 111
        },
        {
            "id": "310da11f-d0e1-481a-8a78-0faff5173d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 117
        },
        {
            "id": "acc59860-7d66-4c1e-a532-16031366899e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 104
        },
        {
            "id": "356c83cd-e667-4c77-9e82-e32fa24edd35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 105
        },
        {
            "id": "a9aa62bb-b3a2-4e87-ab54-0add1fd4156a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "0d73e637-9061-4846-8ff6-9a0c634264ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "86439717-f451-469c-b2dc-96b73509995a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 105
        },
        {
            "id": "0a48b33b-197b-48ec-8055-2d6a4bbeff85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 106
        },
        {
            "id": "bb735f11-d528-449a-a9a5-a0f06d7e5464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 109
        },
        {
            "id": "39d51493-9dff-4f44-83e4-678bb981e762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 114
        },
        {
            "id": "958faf95-ed51-4292-9464-c8aae9d20e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 117
        },
        {
            "id": "df885479-dcd0-4025-8d00-c38ff7437474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 118
        },
        {
            "id": "7c063cc0-94af-4a0b-8c9f-e8232b8cc909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 121
        },
        {
            "id": "f48ad179-74e9-46c2-a69e-0a943cbec2d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 109
        },
        {
            "id": "afda5088-51b0-42ca-aff4-33ef43047e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 114
        },
        {
            "id": "a53004e7-4a1b-4435-814e-89e5e2f906b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 115
        },
        {
            "id": "dea848c7-4e68-41e9-86bc-aff6459ad468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 117
        },
        {
            "id": "723c0afa-ae3b-4fb5-9524-f9a5960d9ca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 118
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}