{
    "id": "8a7145ba-f514-4377-9796-336531c1c704",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "CoolBig",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 4,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Absolute",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "34251cb9-0582-4db2-9a84-8baffc5e4aeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 136,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5e31d773-8fd3-477d-9f81-982a968bf442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 136,
                "offset": 28,
                "shift": 72,
                "w": 16,
                "x": 413,
                "y": 416
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "56a0ca88-887c-495d-a5ed-995d6ed614b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 136,
                "offset": 16,
                "shift": 72,
                "w": 40,
                "x": 371,
                "y": 416
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4a105ceb-8c4e-480f-a15e-787ab32ee07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 136,
                "offset": 2,
                "shift": 72,
                "w": 68,
                "x": 301,
                "y": 416
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f7d131f1-8787-42cf-8256-b6b47e3b2c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 136,
                "offset": 10,
                "shift": 72,
                "w": 52,
                "x": 247,
                "y": 416
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4ff2b702-efd1-4267-94e8-53ffde65bae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 136,
                "offset": 0,
                "shift": 72,
                "w": 73,
                "x": 172,
                "y": 416
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ef4e0901-9eef-485a-bbb8-b44a6d233577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 136,
                "offset": 3,
                "shift": 72,
                "w": 69,
                "x": 101,
                "y": 416
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "693e1a33-9328-42ac-8090-9ca22b79b378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 136,
                "offset": 19,
                "shift": 23,
                "w": 17,
                "x": 82,
                "y": 416
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4bf4d265-f9a9-4327-9e7c-a280286fbef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 136,
                "offset": 17,
                "shift": 72,
                "w": 38,
                "x": 42,
                "y": 416
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b47ee984-8b30-4bf1-9aed-bdd65d6a5f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 136,
                "offset": 17,
                "shift": 72,
                "w": 38,
                "x": 2,
                "y": 416
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b02bfd07-02b3-4433-9406-b71cedfbe929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 136,
                "offset": 7,
                "shift": 72,
                "w": 58,
                "x": 431,
                "y": 416
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "66d0453f-e455-410d-a845-72ac2210f5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 136,
                "offset": 10,
                "shift": 46,
                "w": 28,
                "x": 992,
                "y": 278
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5392a70a-862b-49a4-b4c8-d8d14efa57db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 136,
                "offset": 2,
                "shift": 23,
                "w": 15,
                "x": 916,
                "y": 278
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "30ca392c-0f87-4fdc-84ee-913314d8be12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 136,
                "offset": 10,
                "shift": 48,
                "w": 28,
                "x": 886,
                "y": 278
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e8d24ee4-89ad-4202-a89b-31e7f9f00ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 136,
                "offset": 5,
                "shift": 23,
                "w": 12,
                "x": 872,
                "y": 278
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e14b45dd-fb55-4557-8617-a7177658bf27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 136,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 833,
                "y": 278
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "28f37885-5e20-4450-8259-f17fa42e0c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 136,
                "offset": 2,
                "shift": 47,
                "w": 44,
                "x": 787,
                "y": 278
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4988d5c0-66e3-4aac-bcb6-133f17fe0c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 136,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 753,
                "y": 278
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "99f598c5-e09d-4b43-8193-364337de2556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 136,
                "offset": 2,
                "shift": 47,
                "w": 42,
                "x": 709,
                "y": 278
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7f1aa316-6375-4339-a864-c10cf90c972d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 136,
                "offset": 2,
                "shift": 50,
                "w": 44,
                "x": 663,
                "y": 278
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7b971611-d1a0-4c4c-bd4e-c818dd278388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 136,
                "offset": 1,
                "shift": 51,
                "w": 48,
                "x": 613,
                "y": 278
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "22db7222-4b7c-4ba0-826f-84249208788c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 136,
                "offset": 2,
                "shift": 60,
                "w": 57,
                "x": 933,
                "y": 278
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fa88f967-5dc9-47a2-866f-482749098e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 136,
                "offset": 2,
                "shift": 44,
                "w": 40,
                "x": 586,
                "y": 416
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e4c9d56d-acf1-400a-8351-008d54a5d84a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 136,
                "offset": 4,
                "shift": 53,
                "w": 49,
                "x": 460,
                "y": 554
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "89c369dd-2c12-4b58-bd2f-d2fe3760f017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 136,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 628,
                "y": 416
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bcd8a058-e831-4b49-9e49-bda3d1459848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 136,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 2,
                "y": 692
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f3b75131-1107-4db8-b5f6-6f5cd815172e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 136,
                "offset": 2,
                "shift": 23,
                "w": 22,
                "x": 965,
                "y": 554
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a7721a9b-2fe2-4b82-9bf5-f92e0e9fbc1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 136,
                "offset": 24,
                "shift": 72,
                "w": 20,
                "x": 943,
                "y": 554
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "cf96678e-b7e3-40af-b4c7-bee515128478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 136,
                "offset": 8,
                "shift": 72,
                "w": 55,
                "x": 886,
                "y": 554
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a3e6b7c7-361f-4e31-a666-d58ab0c10a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 136,
                "offset": 10,
                "shift": 48,
                "w": 28,
                "x": 856,
                "y": 554
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d7b275d7-5eaf-419f-87e6-4a898cacfa97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 136,
                "offset": 8,
                "shift": 72,
                "w": 55,
                "x": 799,
                "y": 554
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b2fdbe2d-e5a4-4db2-8d11-cdbdcf9cc2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 136,
                "offset": 9,
                "shift": 72,
                "w": 54,
                "x": 743,
                "y": 554
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2070c22b-f75a-451a-b49b-3a6e212e8643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 136,
                "offset": 1,
                "shift": 72,
                "w": 70,
                "x": 671,
                "y": 554
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a574885a-cf50-4fe1-94e6-18e6e36bce57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 136,
                "offset": -2,
                "shift": 97,
                "w": 102,
                "x": 567,
                "y": 554
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8c9eba4b-a92c-4923-b0dd-44de35d0b655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 136,
                "offset": 4,
                "shift": 75,
                "w": 83,
                "x": 49,
                "y": 692
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "dd6f4892-d4e3-4e88-ae2f-562c40f6a5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 136,
                "offset": 2,
                "shift": 47,
                "w": 54,
                "x": 511,
                "y": 554
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "312dfbd2-ab07-487e-b443-ed970ceae698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 136,
                "offset": 3,
                "shift": 90,
                "w": 81,
                "x": 377,
                "y": 554
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "625f13dc-6005-4521-b47a-6246b6c9910e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 136,
                "offset": 3,
                "shift": 79,
                "w": 91,
                "x": 284,
                "y": 554
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1de7e9ca-034c-41cc-96a6-7545bd83e437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 136,
                "offset": 5,
                "shift": 66,
                "w": 91,
                "x": 191,
                "y": 554
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e6dc8937-556c-4748-82fb-d7a83133d81d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 136,
                "offset": 3,
                "shift": 63,
                "w": 56,
                "x": 133,
                "y": 554
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dcdd6699-8271-4339-a104-ad8cc4724c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 136,
                "offset": 1,
                "shift": 65,
                "w": 71,
                "x": 60,
                "y": 554
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d7469651-7a53-4c58-bb70-a4cdaf66f6c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 136,
                "offset": 3,
                "shift": 32,
                "w": 56,
                "x": 2,
                "y": 554
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "006d9781-5a2f-4efe-9c63-03c49ba78b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 136,
                "offset": 3,
                "shift": 53,
                "w": 77,
                "x": 898,
                "y": 416
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "86ea8229-d346-4ac7-aa29-659a8ab26fb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 136,
                "offset": 4,
                "shift": 54,
                "w": 112,
                "x": 784,
                "y": 416
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b386b0d1-49bc-4179-b559-6f2521e4ab27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 136,
                "offset": 2,
                "shift": 56,
                "w": 104,
                "x": 678,
                "y": 416
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "99c420e5-7b91-4730-b6e6-c3b1070fa8e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 136,
                "offset": 2,
                "shift": 70,
                "w": 70,
                "x": 541,
                "y": 278
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f67fb1ec-d369-4de8-9fc1-d6819dc31144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 136,
                "offset": 3,
                "shift": 95,
                "w": 93,
                "x": 491,
                "y": 416
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b50ba71b-679d-4d92-a918-522b05fbe389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 136,
                "offset": 2,
                "shift": 50,
                "w": 47,
                "x": 492,
                "y": 278
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e3586c2a-3764-447a-8c09-65d0b57e0c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 136,
                "offset": 0,
                "shift": 90,
                "w": 91,
                "x": 306,
                "y": 140
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2766517d-e9ec-45a0-88b9-a076d25bd0b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 136,
                "offset": -5,
                "shift": 51,
                "w": 56,
                "x": 191,
                "y": 140
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0ceae481-4621-4054-8f5c-ec1c98072c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 136,
                "offset": 1,
                "shift": 94,
                "w": 95,
                "x": 94,
                "y": 140
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cc90047b-f21c-4501-8f73-ae7eb737a51b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 136,
                "offset": 3,
                "shift": 81,
                "w": 90,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "da4d163a-83c8-4448-af2b-b7a3d29a20c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 136,
                "offset": 0,
                "shift": 44,
                "w": 91,
                "x": 905,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0e4e5c22-2332-48ae-9e3f-85f68eef5343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 136,
                "offset": 3,
                "shift": 63,
                "w": 62,
                "x": 841,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d3d725b0-c56f-4ae1-b5d0-71d2dc0cf020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 136,
                "offset": 3,
                "shift": 55,
                "w": 56,
                "x": 783,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4efaacde-6951-4b56-a88e-16bf22bde279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 136,
                "offset": 3,
                "shift": 78,
                "w": 80,
                "x": 701,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8c596a36-8f6f-405a-aecf-2d253b4b92fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 136,
                "offset": 1,
                "shift": 80,
                "w": 84,
                "x": 615,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "72a1db9d-6b2d-4486-a26d-1ae9665d238d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 136,
                "offset": -41,
                "shift": 56,
                "w": 107,
                "x": 506,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cef136f5-1c63-403d-8d33-7e38314ae8da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 136,
                "offset": 1,
                "shift": 51,
                "w": 55,
                "x": 249,
                "y": 140
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b484d794-d75e-43c6-bd9f-4e31ea536e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 136,
                "offset": 24,
                "shift": 72,
                "w": 32,
                "x": 472,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8e55eae8-d028-424b-ab34-c68a45220789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 136,
                "offset": 12,
                "shift": 72,
                "w": 48,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "26f52d03-9142-4c20-9747-722fe039afac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 136,
                "offset": 16,
                "shift": 72,
                "w": 32,
                "x": 356,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0d943f72-59b6-4639-9f78-2d2a8424490e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 136,
                "offset": 6,
                "shift": 72,
                "w": 60,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c106b3f0-5404-4e7e-be5f-2daf1ea415c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 136,
                "offset": -1,
                "shift": 72,
                "w": 74,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4dbe6740-22c3-4202-b531-036be0124d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 136,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8fe3868f-986b-422c-a774-0be2fa619e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 136,
                "offset": -5,
                "shift": 35,
                "w": 43,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "068efa78-bdf4-4029-bc4e-ce36e1e708d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 136,
                "offset": -5,
                "shift": 30,
                "w": 45,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "25e9f841-ca15-485f-83a2-8fd687f5da3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 136,
                "offset": -2,
                "shift": 24,
                "w": 29,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b1ea4b7d-e7c3-4982-8cd1-56e27f19dca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 136,
                "offset": -5,
                "shift": 33,
                "w": 41,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "02c43552-9967-4ba2-9d9d-141961a2eee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 136,
                "offset": -3,
                "shift": 24,
                "w": 30,
                "x": 440,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d34ee793-705a-4404-bea3-46c65110d08f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 136,
                "offset": -6,
                "shift": 19,
                "w": 33,
                "x": 399,
                "y": 140
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "72f26f26-4a63-4080-b32e-2527b022399f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 136,
                "offset": -26,
                "shift": 34,
                "w": 63,
                "x": 886,
                "y": 140
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1c7373bb-e068-4c58-8027-6c9116c4fc56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 136,
                "offset": -3,
                "shift": 34,
                "w": 40,
                "x": 434,
                "y": 140
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7797f3f9-bdd4-4937-8bad-0580eb774da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 136,
                "offset": -5,
                "shift": 17,
                "w": 25,
                "x": 382,
                "y": 278
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "83f92d33-efd4-47a1-8b77-d976158d1a43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 136,
                "offset": -42,
                "shift": 17,
                "w": 62,
                "x": 318,
                "y": 278
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6985fcab-16bb-4244-837f-e044ed378065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 136,
                "offset": -4,
                "shift": 38,
                "w": 45,
                "x": 271,
                "y": 278
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e8b9292b-23ef-4142-b1e7-5bc4cdb1559e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 136,
                "offset": -5,
                "shift": 17,
                "w": 25,
                "x": 244,
                "y": 278
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4b0c7df1-55da-4631-8d93-5c381fbdb75c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 136,
                "offset": -4,
                "shift": 50,
                "w": 60,
                "x": 182,
                "y": 278
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cc6473bd-300b-46f5-a113-478711f65d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 136,
                "offset": -4,
                "shift": 35,
                "w": 42,
                "x": 138,
                "y": 278
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f52d8ecc-f15c-4484-ba3d-b9e675447cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 136,
                "offset": -4,
                "shift": 29,
                "w": 41,
                "x": 95,
                "y": 278
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "191b2578-90fd-4464-9b77-cefcfa890783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 136,
                "offset": -7,
                "shift": 30,
                "w": 48,
                "x": 45,
                "y": 278
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3de03ae0-c196-4f78-8bbc-65f8c48f2e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 136,
                "offset": -5,
                "shift": 33,
                "w": 41,
                "x": 2,
                "y": 278
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "64d3d432-f16f-4013-8800-bc24490f5e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 136,
                "offset": -6,
                "shift": 30,
                "w": 33,
                "x": 409,
                "y": 278
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a2ef789f-cb98-4d3e-a65b-ee7cb725518a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 136,
                "offset": -11,
                "shift": 27,
                "w": 44,
                "x": 951,
                "y": 140
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8ed6d2fd-f90a-43a7-930c-150fdf4ae2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 136,
                "offset": -8,
                "shift": 17,
                "w": 75,
                "x": 809,
                "y": 140
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ccf01ded-24cf-49d2-b27e-442925b9bdb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 136,
                "offset": -4,
                "shift": 34,
                "w": 41,
                "x": 766,
                "y": 140
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5c43973c-03a7-4dd5-bf76-e6011596efd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 136,
                "offset": -4,
                "shift": 26,
                "w": 32,
                "x": 732,
                "y": 140
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4c9b1ddc-3ac7-4a4c-a755-124ae681e774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 136,
                "offset": -3,
                "shift": 45,
                "w": 47,
                "x": 683,
                "y": 140
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6df234ad-6986-4776-b52c-ae494c333723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 136,
                "offset": -9,
                "shift": 28,
                "w": 39,
                "x": 642,
                "y": 140
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "46bfa4c3-b171-45a5-8fe2-e3117d20de1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 136,
                "offset": -24,
                "shift": 35,
                "w": 62,
                "x": 578,
                "y": 140
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "76d52474-6e1f-460d-8e90-94916e06ce26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 136,
                "offset": -6,
                "shift": 35,
                "w": 40,
                "x": 536,
                "y": 140
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "41ad0a8f-4de2-4eec-b7a7-228c7c7ee11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 136,
                "offset": 13,
                "shift": 72,
                "w": 46,
                "x": 488,
                "y": 140
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bc9afac0-fa4c-4ae5-840f-9ad0e6afa1e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 136,
                "offset": 31,
                "shift": 72,
                "w": 10,
                "x": 476,
                "y": 140
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5fff62f3-e31d-487d-8278-d1299a21c5b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 136,
                "offset": 13,
                "shift": 72,
                "w": 46,
                "x": 444,
                "y": 278
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "753a73e8-0f09-4eef-9f3d-4a58d49bf6f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 136,
                "offset": 8,
                "shift": 72,
                "w": 55,
                "x": 134,
                "y": 692
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a02a36e5-4361-445c-bf2b-694083be13c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 49
        },
        {
            "id": "c7da386c-f338-4867-a565-50f5229c5bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 52
        },
        {
            "id": "3acb0531-9084-4a79-b796-c37656dd7590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 49
        },
        {
            "id": "15aee7be-3546-4fba-9beb-6f858e807746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 53,
            "second": 48
        },
        {
            "id": "d3743922-c3f8-458e-934c-b9f5c2cef7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 53,
            "second": 49
        },
        {
            "id": "e3c4bb78-2a32-451d-b551-5638c684a3b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 53,
            "second": 50
        },
        {
            "id": "db80415c-b980-459f-bc65-2bc119f0a47e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 53,
            "second": 51
        },
        {
            "id": "99b49890-926b-4650-9606-b256327d4e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 53,
            "second": 52
        },
        {
            "id": "bceff9ef-a1ec-45d0-a3a2-66f61c3912be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 53,
            "second": 53
        },
        {
            "id": "78ad42b0-5fe8-4dbf-b1b1-5798a6416d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 53,
            "second": 54
        },
        {
            "id": "4a0d587b-71a5-4930-9d99-311d73ff9cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 55
        },
        {
            "id": "64d949ca-6ae4-47c5-924a-bfeae36f9041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 53,
            "second": 56
        },
        {
            "id": "ec407cbf-9c47-4c4b-b7a9-5f99d2f8a5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 53,
            "second": 57
        },
        {
            "id": "da5156f8-0c9f-41f4-ace4-aa109abaf4e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 48
        },
        {
            "id": "de2a8df2-f605-4183-b627-3258c7c2e5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 50
        },
        {
            "id": "41cef10a-d0f5-47f3-91b4-c0bce25e1841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 51
        },
        {
            "id": "e2632f0b-2959-4031-9449-fc26263d6e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 55,
            "second": 52
        },
        {
            "id": "93d5a847-f341-4ed3-b9e9-ec222c07641b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 54
        },
        {
            "id": "598801a7-f6d9-49a7-a43c-580cd3dedba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 55,
            "second": 56
        },
        {
            "id": "ba10309f-e5dc-4dfb-b92e-327060fc316f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 57
        },
        {
            "id": "644889c8-830b-4052-8465-1b04426c9019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 48
        },
        {
            "id": "eee92e88-ddbe-4121-a014-34970dedcc0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 50
        },
        {
            "id": "e6906fac-f1a2-4c4f-8c30-44b1b2f44b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 54
        },
        {
            "id": "265622b0-e55b-47b1-b75d-413da1b78582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 56,
            "second": 56
        },
        {
            "id": "16244163-47b4-466d-bb28-7adc7de20654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 56,
            "second": 57
        },
        {
            "id": "6a36052a-083a-4a17-a8ae-37193e531fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 56
        },
        {
            "id": "2eb6f03a-9725-4185-8509-cb54847c1051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 97
        },
        {
            "id": "eb2ae7e3-44de-4825-8f93-20dd0b86fc9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 101
        },
        {
            "id": "44808145-11a6-42c8-94b4-3acd077fc301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 111
        },
        {
            "id": "52e5dd4a-b996-4e77-b792-bbbcfbb21832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 117
        },
        {
            "id": "d02e7e93-36f3-48ba-a886-bf1672d55df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 97
        },
        {
            "id": "05358188-6c57-44a3-9911-fc35c5778ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 101
        },
        {
            "id": "e71b1b6b-f2bf-44c8-8ddc-08b60ec28813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 83,
            "second": 116
        },
        {
            "id": "0700e777-842e-4594-9e01-4f12a2a3e2cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 87,
            "second": 104
        },
        {
            "id": "cd25aa2e-fb2b-4c8a-ae19-fc917b0ee65f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 87,
            "second": 105
        },
        {
            "id": "5e924c77-0a6a-4d96-afd3-203968a09ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 98
        },
        {
            "id": "a1db5a4d-352c-4f4e-8ffc-0c86b8b67aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 101
        },
        {
            "id": "088f8a25-2e73-44b6-83f1-f79edd3c1e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 107,
            "second": 115
        },
        {
            "id": "bcd3ca04-6ca9-459a-8f4d-2ce5b0ec7115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 115
        },
        {
            "id": "540a20d8-f291-4efd-bfc9-96f3a9b9e266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 112,
            "second": 108
        },
        {
            "id": "cd5eb958-3627-45ca-a570-d9a2f075f68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 112
        },
        {
            "id": "e271ba21-f6f1-4a57-9568-050b70498d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 116
        },
        {
            "id": "597e72a6-ad79-4edd-bdf2-2efbd4c2583c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 117
        },
        {
            "id": "c0efe0a3-3898-485c-9f76-eee992d4e4be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 118
        },
        {
            "id": "66977d43-9b9b-4be7-a03d-95dd5d5c4523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 119
        },
        {
            "id": "c4ba9f81-a735-4efc-872e-ced59b50d3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 121
        },
        {
            "id": "b732d406-e0a6-4486-8183-09245db4d9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 114,
            "second": 100
        },
        {
            "id": "0dcd2034-cde1-4a82-829e-3b039db76fdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 103
        },
        {
            "id": "a39417f0-6bdf-4750-94dc-cd6b751944b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 113
        },
        {
            "id": "2cf76f6b-9f46-4ad7-b874-c3a3030316ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 114,
            "second": 115
        },
        {
            "id": "0ccfd8c4-77ab-45d1-bd8a-826242eb9ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 120
        },
        {
            "id": "79c74511-dff7-411a-9c30-c34ab6548a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 98
        },
        {
            "id": "0841fe64-12b9-4c8c-b249-e2311b79f465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 118,
            "second": 104
        },
        {
            "id": "d4f88f41-325b-4914-9f5f-a35ad627c0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 118,
            "second": 105
        },
        {
            "id": "b0bcea40-197e-4891-a43f-b9cc56270dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 118,
            "second": 106
        },
        {
            "id": "1f256a84-d09d-4dc3-9a32-88d316857971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 107
        },
        {
            "id": "0bb7dba0-54fb-4f04-bdf6-f8f27cee7c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 108
        },
        {
            "id": "623b6e73-4ece-4dfc-a70a-622a9525347b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 118,
            "second": 109
        },
        {
            "id": "817b4416-d3ad-4f59-ba0b-f6cbf2ece5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 118,
            "second": 110
        },
        {
            "id": "911cfbc6-39cd-4f51-84c0-0fa936615551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 112
        },
        {
            "id": "208c6c18-b515-41d5-a7be-2ec454812f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 118,
            "second": 114
        },
        {
            "id": "c074903d-0a5e-4807-8765-550584ab6382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 118,
            "second": 116
        },
        {
            "id": "ff43cf98-6dc9-45b9-9e68-3d23be76b68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 118,
            "second": 117
        },
        {
            "id": "17c53309-d498-4e3d-a8df-9f2eea0b4116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 118,
            "second": 118
        },
        {
            "id": "c0bcb516-2fb5-4bb1-aaf5-b2e23227aa31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 118,
            "second": 119
        },
        {
            "id": "63490c61-05e2-4b5e-9dc6-ec0fb68c9893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 120
        },
        {
            "id": "430432de-0ba6-4918-9300-c761e19c8220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 118,
            "second": 121
        },
        {
            "id": "a0270e0c-9f30-4d3d-90f3-7a478799745b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 119,
            "second": 109
        },
        {
            "id": "aacba86f-cef3-46c3-a1eb-741cba6b8708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 110
        },
        {
            "id": "1068c40a-ce6e-4f72-abed-aca757041776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 113
        },
        {
            "id": "e7f1e696-7943-4641-b2f3-024abbcc4a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 114
        },
        {
            "id": "d005abc3-071a-443d-a21c-308b32c48283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 115
        },
        {
            "id": "05c474fe-1c9e-4f88-85d7-614cdb8fdcb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 117
        },
        {
            "id": "3642adcb-4261-4e92-9faa-0755dcdb27ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 120,
            "second": 98
        },
        {
            "id": "d74ec5fd-3e68-41fa-9422-7d126ef42627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 120,
            "second": 109
        },
        {
            "id": "cebd799e-1c9e-4fa7-b77c-1bed5604e90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 120,
            "second": 110
        },
        {
            "id": "4337a644-3190-4eed-af70-aa9d8aa536bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 120,
            "second": 112
        },
        {
            "id": "03d6a1a3-8328-4ad2-b1dd-cc76c538f03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 120,
            "second": 114
        },
        {
            "id": "537480bb-1367-4fdd-80ae-7a7581f03718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 120,
            "second": 115
        },
        {
            "id": "a74b269c-3671-42b9-976e-a03d657c3fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 120,
            "second": 117
        },
        {
            "id": "8fd6dc3b-4250-45a0-babd-68e6537dd716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 120,
            "second": 118
        },
        {
            "id": "7bc404e5-6bfa-42cd-8f16-c30d31182676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 120,
            "second": 119
        },
        {
            "id": "69338b86-2e0b-43c3-8531-fc1acac9199b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 120,
            "second": 121
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 90,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}