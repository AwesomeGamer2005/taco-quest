{
    "id": "e9534c33-3a02-47bf-9673-10623f3066dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ArcherTaco_obj",
    "eventList": [
        {
            "id": "3b14a3db-899d-4a34-b6ed-cefc87ef6788",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9534c33-3a02-47bf-9673-10623f3066dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6e46ffc9-3520-4b70-a6e7-8c62b8ebc7a7",
    "visible": true
}