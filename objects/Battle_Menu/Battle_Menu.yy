{
    "id": "0e6e7c14-4940-48f6-b935-64e528b53587",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Battle_Menu",
    "eventList": [
        {
            "id": "293059e5-e8d9-41f8-aac6-1d8f11524327",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e6e7c14-4940-48f6-b935-64e528b53587"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "08783562-bb6f-4b5d-a6b3-3a0a483c93df",
    "visible": true
}