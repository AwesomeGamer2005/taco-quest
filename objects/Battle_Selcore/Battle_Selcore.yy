{
    "id": "7cb10c46-fb90-4eab-927a-208f44a044ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Battle_Selcore",
    "eventList": [
        {
            "id": "1da136d5-499f-4f02-87e8-4fdaba9b3e5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7cb10c46-fb90-4eab-927a-208f44a044ca"
        },
        {
            "id": "733020f1-0155-432b-ace6-467696db76fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7cb10c46-fb90-4eab-927a-208f44a044ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e48141e-c5e8-4a17-a30b-716760735e8e",
    "visible": true
}