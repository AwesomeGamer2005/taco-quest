//Movement
if keyboard_check_pressed(vk_right) && selected = 1
{
	x += 566
	selected = 2
}
if keyboard_check_pressed(vk_left) && selected = 2
{
	x -= 566
	selected = 1
}

//Selection
if keyboard_check_pressed(vk_enter)
{
	if selected = 1
	{
		global.menu = "Item"
	}
	
	if selected = 2
	{
		global.menu = "Attack"
	}
}