{
    "id": "edf56f82-c8e8-4e6e-bdca-2680eac03828",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "KnightTaco_obj",
    "eventList": [
        {
            "id": "36929b69-2369-480b-b606-d7a51321a6b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "edf56f82-c8e8-4e6e-bdca-2680eac03828"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35c654a6-81f7-45c5-975c-0316d2cdb380",
    "visible": true
}