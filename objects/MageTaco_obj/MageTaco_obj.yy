{
    "id": "aff628c3-47b9-46e3-8766-1a7ecd60eed2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MageTaco_obj",
    "eventList": [
        {
            "id": "425dcafb-02a0-4cb4-9767-35e00803040c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aff628c3-47b9-46e3-8766-1a7ecd60eed2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "70f187a7-3126-4060-9324-52dcf94db69a",
    "visible": true
}