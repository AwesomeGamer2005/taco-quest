{
    "id": "2caf932b-115d-480f-ac35-a256ffe1b2da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Menu_obj",
    "eventList": [
        {
            "id": "8344a8f4-ec7c-4108-b4e6-687bac7ce0e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2caf932b-115d-480f-ac35-a256ffe1b2da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cbe32fd5-5da5-4f44-a385-95ce31b28133",
    "visible": true
}