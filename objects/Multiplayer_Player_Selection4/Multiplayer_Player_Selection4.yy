{
    "id": "a1e8d56d-0e51-45d0-9d17-d760c5a23eef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Multiplayer_Player_Selection4",
    "eventList": [
        {
            "id": "78604489-2474-4a69-aac1-895ebc31d66d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1e8d56d-0e51-45d0-9d17-d760c5a23eef"
        },
        {
            "id": "1257c91b-d5c7-452c-8b00-edb96d4eb219",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a1e8d56d-0e51-45d0-9d17-d760c5a23eef"
        },
        {
            "id": "0a706676-27be-42ed-9515-cb2f3d4bd0c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a1e8d56d-0e51-45d0-9d17-d760c5a23eef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}