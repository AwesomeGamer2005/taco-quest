//Lets Players Choose Their Character One at a Time
if p1 = 0 && p2 = 0 && p3 = 0 && p4 = 0
{
	if !instance_exists(Player1_selector_multiplayer)
	{
		instance_create_layer(32,288,"Instances",Player1_selector_multiplayer)
	}
	P = 1
}
if p1 = 1 && p2 = 0 && p3 = 0 && p4 = 0
{
	instance_destroy(Player1_selector_multiplayer)
	if !instance_exists(Player2_selector)
	{
		instance_create_layer(32,288,"Instances",Player2_selector)
	}
	P = 2
}
if p1 = 1 && p2 = 1 && p3 = 0 && p4 = 0
{
	instance_destroy(Player2_selector)
	if !instance_exists(Player3_selector)
	{
		instance_create_layer(32,288,"Instances",Player3_selector)
	}
	P = 3
}
if p1 = 1 && p2 = 1 && p3 = 1 && p4 = 0
{
	instance_destroy(Player3_selector)
	if !instance_exists(Player4_selector)
	{
		instance_create_layer(32,288,"Instances",Player4_selector)
	}
	P = 4
}
if p1 = 1 && p2 = 1 && p3 = 1 && p4 = 1
{
	audio_stop_sound(Song1)
	instance_destroy(Player4_selector)
	room_goto(Battle)
}