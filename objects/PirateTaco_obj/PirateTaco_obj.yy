{
    "id": "da3b2d14-408b-422c-8b6d-eb73fb872ffe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PirateTaco_obj",
    "eventList": [
        {
            "id": "0fe34a51-c999-4f7f-b8b6-69dcabfba9d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "da3b2d14-408b-422c-8b6d-eb73fb872ffe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c2e3700-925f-4974-84ac-51cdc8124afb",
    "visible": true
}