{
    "id": "b6328df3-e64a-42f9-a862-22d9156bc74a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Player1_selector",
    "eventList": [
        {
            "id": "ca6667cf-7134-4034-84b3-7a519d1e6e0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b6328df3-e64a-42f9-a862-22d9156bc74a"
        },
        {
            "id": "57fe464b-3021-499f-860c-a662f0a35609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6328df3-e64a-42f9-a862-22d9156bc74a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fba3c26a-2329-47ec-8934-97c9455e78fd",
    "visible": true
}