{
    "id": "68d2f10d-be14-4c73-8a7c-27a90124648e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Player1_selector_multiplayer",
    "eventList": [
        {
            "id": "10df719f-007c-4eaa-9599-4c6e125b673d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68d2f10d-be14-4c73-8a7c-27a90124648e"
        },
        {
            "id": "c613da07-6550-4a39-9efd-1bd6e8ce7967",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68d2f10d-be14-4c73-8a7c-27a90124648e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fba3c26a-2329-47ec-8934-97c9455e78fd",
    "visible": true
}