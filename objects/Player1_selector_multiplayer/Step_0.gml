//Movement
if keyboard_check_pressed(vk_right) && char < 5
{
	x += 192
	char += 1
}

if keyboard_check_pressed(vk_left) && char > 1
{
	x -= 192
	char -= 1
}
//Selection
if keyboard_check_pressed(vk_enter)
{
	if char = 1
	{
		global.char1 = "Mage"
	}
	if char = 2
	{
		global.char1 = "Knight"
	}
	if char = 3
	{
		global.char1 = "Cyborg"
	}
	if char = 4
	{
		global.char1 = "Pirate"
	}
	if char = 5
	{
		global.char1 = "Archer"
	}
	if global.pp = 2
	{
		Multiplayer_Player_Selection2.p1 = 1
	}
	if global.pp = 3
	{
		Multiplayer_Player_Selection3.p1 = 1
	}
	if global.pp = 4
	{
		Multiplayer_Player_Selection4.p1 = 1
	}
}