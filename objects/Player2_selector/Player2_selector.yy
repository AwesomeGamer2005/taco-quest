{
    "id": "4d5077e4-9bdb-4d6e-b3b5-b03a5f499f43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Player2_selector",
    "eventList": [
        {
            "id": "215d9228-6836-43b3-9fb5-101dfd72e8f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d5077e4-9bdb-4d6e-b3b5-b03a5f499f43"
        },
        {
            "id": "427b14bf-9309-4d35-9410-b8f14bd79745",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d5077e4-9bdb-4d6e-b3b5-b03a5f499f43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fba3c26a-2329-47ec-8934-97c9455e78fd",
    "visible": true
}