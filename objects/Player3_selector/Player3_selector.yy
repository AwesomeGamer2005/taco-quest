{
    "id": "68a218b3-5a0c-4ed9-9fe3-e88ddb498b5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Player3_selector",
    "eventList": [
        {
            "id": "0b861aa0-c258-4530-86d5-aaeabfc0b868",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68a218b3-5a0c-4ed9-9fe3-e88ddb498b5a"
        },
        {
            "id": "a6aacc9c-cbcd-4c0d-9558-72226faa118e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68a218b3-5a0c-4ed9-9fe3-e88ddb498b5a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fba3c26a-2329-47ec-8934-97c9455e78fd",
    "visible": true
}