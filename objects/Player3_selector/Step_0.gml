//Movement
if keyboard_check_pressed(vk_right) && char < 5
{
	x += 192
	char += 1
}

if keyboard_check_pressed(vk_left) && char > 1
{
	x -= 192
	char -= 1
}
//Selection
if keyboard_check_pressed(vk_enter)
{
	if char = 1 && global.magelock = 0
	{
		global.char3 = "Mage"
	}
	if char = 2 && global.knightlock = 0
	{
		global.char3 = "Knight"
	}
	if char = 3 && global.cyborglock = 0
	{
		global.char3 = "Cyborg"
	}
	if char = 4 && global.piratelock = 0
	{
		global.char3 = "Pirate"
	}
	if char = 5 && global.archerlock = 0
	{
		global.char3 = "Archer"
	}
	if global.char3 != "N/A"
	{
		if global.pp = 2
		{
			Multiplayer_Player_Selection2.p3 = 1
		}
		if global.pp = 3
		{
			Multiplayer_Player_Selection3.p3 = 1
		}
		if global.pp = 4
		{
			Multiplayer_Player_Selection4.p3 = 1
		}
	}
}