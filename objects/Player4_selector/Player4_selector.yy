{
    "id": "ab6d2306-44e9-42c5-a933-cd469f19dd2d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Player4_selector",
    "eventList": [
        {
            "id": "795f9202-b3e1-4e23-8834-6d2a2f24487f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab6d2306-44e9-42c5-a933-cd469f19dd2d"
        },
        {
            "id": "9f3a2837-4afd-4909-849f-e549cbc48ec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab6d2306-44e9-42c5-a933-cd469f19dd2d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fba3c26a-2329-47ec-8934-97c9455e78fd",
    "visible": true
}