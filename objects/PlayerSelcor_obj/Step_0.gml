//Movement
if keyboard_check_pressed(vk_up)
{
	if option > 0
	{
		y -= 130
		option -= 1
	}
}
if keyboard_check_pressed(vk_down)
{
	if option < 2
	{
		y += 130
		option += 1
	}
}

//Selection
if keyboard_check_pressed(vk_enter)
{
	if option = 0
	{
		global.players = 2
		room_goto(CharSelectMultiplayer2)
	}
	
	if option = 1
	{
		global.players = 3
		room_goto(CharSelectMultiplayer3)
	}
	
	if option = 2
	{
		global.players = 4
		room_goto(CharSelectMultiplayer4)
	}
}