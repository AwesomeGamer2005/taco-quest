{
    "id": "e8f7bb91-3aea-4308-9787-4abdb200e4b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Selcor_obj",
    "eventList": [
        {
            "id": "e248684d-0b05-4d2f-8a4b-f2dea7ec273d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8f7bb91-3aea-4308-9787-4abdb200e4b0"
        },
        {
            "id": "0d654b63-e8a1-411a-89e4-1a80b6ba13f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8f7bb91-3aea-4308-9787-4abdb200e4b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8510e358-e88b-4b30-87d5-3b9ab02c9345",
    "visible": true
}