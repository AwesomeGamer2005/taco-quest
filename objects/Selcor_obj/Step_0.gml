//Movement
if keyboard_check_pressed(vk_up)
{
	if option > 0
	{
		y -= 70
		option -= 1
	}
}
if keyboard_check_pressed(vk_down)
{
	if option < 2
	{
		y += 70
		option += 1
	}
}

//Selection
if keyboard_check_pressed(vk_enter)
{
	if option = 0
	{
		room_goto(CharSelectSingleplayer)
	}
	
	if option = 1
	{
		room_goto(PlayerNumberSelectMultiplayer)
	}
	
	if option = 2
	{
		game_end()
	}
}