{
    "id": "6b58c715-d39c-4d45-9f09-ed8f0a5de5a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Title_obj",
    "eventList": [
        {
            "id": "378bee9e-88e6-4b99-980f-321f4a7ada2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b58c715-d39c-4d45-9f09-ed8f0a5de5a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "88b546fb-9654-4dca-9066-c594f5abed42",
    "visible": true
}