{
    "id": "55c8d54c-a49c-486f-945f-24ff47a053e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Archer_locked_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60dd692d-d323-4095-97a0-63ca6c8c0cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55c8d54c-a49c-486f-945f-24ff47a053e2",
            "compositeImage": {
                "id": "fc1d2122-6eb7-4026-8cec-dccac9b3ad06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60dd692d-d323-4095-97a0-63ca6c8c0cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbccfa9e-a93b-4d2b-bb32-1ee285ba5874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60dd692d-d323-4095-97a0-63ca6c8c0cd1",
                    "LayerId": "03f254bd-538c-47bf-aa25-62253ddbf716"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "03f254bd-538c-47bf-aa25-62253ddbf716",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55c8d54c-a49c-486f-945f-24ff47a053e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}