{
    "id": "6e46ffc9-3520-4b70-a6e7-8c62b8ebc7a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Archer_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "643fcc12-2233-4c42-aedb-915f5517086e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e46ffc9-3520-4b70-a6e7-8c62b8ebc7a7",
            "compositeImage": {
                "id": "c92fda0a-7222-496b-99ab-02dd2af1bf42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643fcc12-2233-4c42-aedb-915f5517086e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876c3641-6b65-485f-837b-d9aef4835611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643fcc12-2233-4c42-aedb-915f5517086e",
                    "LayerId": "3a1bdded-41eb-401b-8d73-4bdc162656ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3a1bdded-41eb-401b-8d73-4bdc162656ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e46ffc9-3520-4b70-a6e7-8c62b8ebc7a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}