{
    "id": "77b143f0-7aa4-43b7-894a-fae27fed8454",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Archer_spr_battle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33d25a68-c8e4-4831-89ff-f75ba7b2f388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b143f0-7aa4-43b7-894a-fae27fed8454",
            "compositeImage": {
                "id": "d2c71a04-a1ac-4d86-b6b4-84a6f3c5ec4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d25a68-c8e4-4831-89ff-f75ba7b2f388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e706468-c2df-4532-aa11-0b5a9821f116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d25a68-c8e4-4831-89ff-f75ba7b2f388",
                    "LayerId": "0f63d40a-5a27-492d-ad9d-14a4d7ea976b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0f63d40a-5a27-492d-ad9d-14a4d7ea976b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77b143f0-7aa4-43b7-894a-fae27fed8454",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}