{
    "id": "ff5135c3-9479-44e6-abe9-be95589ee639",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Archer_spr_battle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 3,
    "bbox_right": 63,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e5d139e-1c44-48aa-8786-8eaf556aa357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff5135c3-9479-44e6-abe9-be95589ee639",
            "compositeImage": {
                "id": "e40108de-59a4-43cf-8d89-128f1928ea56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5d139e-1c44-48aa-8786-8eaf556aa357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6c6391e-66a0-4e24-84ba-57f2b2574718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5d139e-1c44-48aa-8786-8eaf556aa357",
                    "LayerId": "3acc1c36-6d0d-4335-87de-8599b7038736"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3acc1c36-6d0d-4335-87de-8599b7038736",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff5135c3-9479-44e6-abe9-be95589ee639",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}