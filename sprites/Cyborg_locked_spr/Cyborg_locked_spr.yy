{
    "id": "c2ca49fc-07ad-4a9b-b93f-fe99bf697999",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cyborg_locked_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 2,
    "bbox_right": 63,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "387676c0-9fed-4b52-98e2-5770757e6ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2ca49fc-07ad-4a9b-b93f-fe99bf697999",
            "compositeImage": {
                "id": "0a895328-848a-41b9-912f-62a901e9d5e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "387676c0-9fed-4b52-98e2-5770757e6ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6988ae02-b3a3-477c-87fd-bf15930ce20c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "387676c0-9fed-4b52-98e2-5770757e6ca7",
                    "LayerId": "2008a49d-ee14-4802-bc04-9ff77a60ed95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2008a49d-ee14-4802-bc04-9ff77a60ed95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2ca49fc-07ad-4a9b-b93f-fe99bf697999",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}