{
    "id": "aa8c1250-dbe0-41e7-84ef-b52a55b8d2ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cyborg_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 2,
    "bbox_right": 63,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46d92edc-c3e6-4f82-b8f8-e6a6063bc20d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa8c1250-dbe0-41e7-84ef-b52a55b8d2ce",
            "compositeImage": {
                "id": "ddfac06e-731d-4a22-84db-48eb33abe362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d92edc-c3e6-4f82-b8f8-e6a6063bc20d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6987f1e3-fe29-48b7-977b-ce8d7d7d4577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d92edc-c3e6-4f82-b8f8-e6a6063bc20d",
                    "LayerId": "7f8ab1ad-959b-42c6-a096-56e3229487ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7f8ab1ad-959b-42c6-a096-56e3229487ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa8c1250-dbe0-41e7-84ef-b52a55b8d2ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}