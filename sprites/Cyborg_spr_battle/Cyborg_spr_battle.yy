{
    "id": "12729db7-fe6b-406f-91a3-15a9ae5b43e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cyborg_spr_battle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 2,
    "bbox_right": 63,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "254e8851-3b36-4ee5-849a-7a4014c2dc47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12729db7-fe6b-406f-91a3-15a9ae5b43e8",
            "compositeImage": {
                "id": "6c595080-22e1-4749-a366-b52a7645b459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254e8851-3b36-4ee5-849a-7a4014c2dc47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0cce350-5dae-4a6c-9580-f88bbe758be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254e8851-3b36-4ee5-849a-7a4014c2dc47",
                    "LayerId": "d18e353a-0e97-4212-9e93-0869a90fca0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d18e353a-0e97-4212-9e93-0869a90fca0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12729db7-fe6b-406f-91a3-15a9ae5b43e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}