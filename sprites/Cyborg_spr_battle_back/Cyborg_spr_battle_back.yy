{
    "id": "9f27552e-b6cc-49e6-82f0-bd6e47025337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cyborg_spr_battle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17c84c05-91b4-4415-a3b4-49ada45cf139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f27552e-b6cc-49e6-82f0-bd6e47025337",
            "compositeImage": {
                "id": "df0b1bd8-01a7-4f54-bc28-3cf22cdb0c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17c84c05-91b4-4415-a3b4-49ada45cf139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be2b2194-3457-4d93-b3d8-f3ec706eff4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17c84c05-91b4-4415-a3b4-49ada45cf139",
                    "LayerId": "1147fa59-96ef-40f6-be3e-3ef6278140b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1147fa59-96ef-40f6-be3e-3ef6278140b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f27552e-b6cc-49e6-82f0-bd6e47025337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}