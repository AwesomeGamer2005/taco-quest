{
    "id": "ea1d68ce-f69f-4bea-b021-026d46c28c57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "DownLeftFloor_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "143dcaad-1387-4ac3-8b18-c2009c596f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea1d68ce-f69f-4bea-b021-026d46c28c57",
            "compositeImage": {
                "id": "728e473e-5d87-466c-8291-6373993f00b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143dcaad-1387-4ac3-8b18-c2009c596f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207fcbe5-ec1f-49aa-af73-8ff064afb3de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143dcaad-1387-4ac3-8b18-c2009c596f28",
                    "LayerId": "ec72f644-ca4a-4818-8876-1805458a51ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ec72f644-ca4a-4818-8876-1805458a51ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea1d68ce-f69f-4bea-b021-026d46c28c57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}