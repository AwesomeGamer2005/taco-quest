{
    "id": "95751138-d51b-4da9-977d-8c59f2576254",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "DownRightFloor_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56f16520-4eba-49da-9c9f-dac927f24da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95751138-d51b-4da9-977d-8c59f2576254",
            "compositeImage": {
                "id": "be18aec2-6a73-4d0d-8b80-c8bcafe4482c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f16520-4eba-49da-9c9f-dac927f24da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d8f710b-5131-46b5-8775-3e21dbe8cd8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f16520-4eba-49da-9c9f-dac927f24da4",
                    "LayerId": "64205bae-b54e-4a54-8784-e867605b4ef3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "64205bae-b54e-4a54-8784-e867605b4ef3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95751138-d51b-4da9-977d-8c59f2576254",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}