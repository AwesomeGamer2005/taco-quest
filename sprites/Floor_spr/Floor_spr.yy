{
    "id": "848b49c0-8155-40dc-b768-5e689e0ba2ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Floor_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13f600b1-8f48-4b6c-9d6e-2aae267fcc67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "848b49c0-8155-40dc-b768-5e689e0ba2ef",
            "compositeImage": {
                "id": "7a3bf262-7da6-43b9-87f2-b88c4042ecb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13f600b1-8f48-4b6c-9d6e-2aae267fcc67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c563ee5-aca0-4dda-bf85-c82610ee78ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13f600b1-8f48-4b6c-9d6e-2aae267fcc67",
                    "LayerId": "178318eb-bc17-4bd9-9ff9-84be317e2ee1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "178318eb-bc17-4bd9-9ff9-84be317e2ee1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "848b49c0-8155-40dc-b768-5e689e0ba2ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}