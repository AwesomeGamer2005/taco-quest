{
    "id": "73681efa-4f4a-4d20-9228-d6c89bb5d880",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HorizontalFloor_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8ad4c4a-c4ac-49c6-82a7-e480003f4ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73681efa-4f4a-4d20-9228-d6c89bb5d880",
            "compositeImage": {
                "id": "8420210d-d3f1-455c-9ff6-45e3339805e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ad4c4a-c4ac-49c6-82a7-e480003f4ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05128573-470b-411d-a90f-9cbb33c5de54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ad4c4a-c4ac-49c6-82a7-e480003f4ba2",
                    "LayerId": "3aae66d3-4b85-4ed4-882b-21d9af1e1c20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3aae66d3-4b85-4ed4-882b-21d9af1e1c20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73681efa-4f4a-4d20-9228-d6c89bb5d880",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}