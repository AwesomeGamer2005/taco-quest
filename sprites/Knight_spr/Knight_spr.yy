{
    "id": "35c654a6-81f7-45c5-975c-0316d2cdb380",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Knight_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc273126-632e-4124-b3d3-0731b0c80e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35c654a6-81f7-45c5-975c-0316d2cdb380",
            "compositeImage": {
                "id": "e390093d-a54a-4ee6-8c0e-54e033fa0613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc273126-632e-4124-b3d3-0731b0c80e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38ac63c-4e5a-4a97-ba7d-d9025bad6066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc273126-632e-4124-b3d3-0731b0c80e25",
                    "LayerId": "99e6d9a0-be50-4975-8a54-4df7c4a106b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "99e6d9a0-be50-4975-8a54-4df7c4a106b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35c654a6-81f7-45c5-975c-0316d2cdb380",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}