{
    "id": "2e1d72b7-aedc-4962-bd96-377e4a4e0056",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Knight_spr_battle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67468946-47b4-4d7f-9acf-a0a13c21587b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e1d72b7-aedc-4962-bd96-377e4a4e0056",
            "compositeImage": {
                "id": "a383e99f-597f-4b87-9306-6c1474ee8721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67468946-47b4-4d7f-9acf-a0a13c21587b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f28d6e-5490-4da5-beef-b94f059687b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67468946-47b4-4d7f-9acf-a0a13c21587b",
                    "LayerId": "c309f903-7ac0-4856-a47b-0c998b584aee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c309f903-7ac0-4856-a47b-0c998b584aee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e1d72b7-aedc-4962-bd96-377e4a4e0056",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}