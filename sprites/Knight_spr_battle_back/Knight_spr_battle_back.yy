{
    "id": "bdf0ed3d-7f3b-4159-8f19-4881c1f90a31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Knight_spr_battle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc69fa6b-cd03-4a64-9fe4-84b38cdc8e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdf0ed3d-7f3b-4159-8f19-4881c1f90a31",
            "compositeImage": {
                "id": "fb86b380-2f7e-4928-b274-2c7996d4f445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc69fa6b-cd03-4a64-9fe4-84b38cdc8e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e826b84-0f50-4252-a4d1-bea6f64b1bc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc69fa6b-cd03-4a64-9fe4-84b38cdc8e4e",
                    "LayerId": "1876eb8d-6796-4117-bce5-def436cfdcf0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1876eb8d-6796-4117-bce5-def436cfdcf0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdf0ed3d-7f3b-4159-8f19-4881c1f90a31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}