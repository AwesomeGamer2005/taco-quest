{
    "id": "a6e84007-1c84-4fdd-a6ea-989ba0014049",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mage_locked_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 5,
    "bbox_right": 60,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e7c3189-8b84-401b-8c7c-480e630619a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e84007-1c84-4fdd-a6ea-989ba0014049",
            "compositeImage": {
                "id": "2522aa7a-7be3-412d-ad5a-ce44a72f9693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7c3189-8b84-401b-8c7c-480e630619a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "393f1f0b-a407-4f0c-a78b-044fa9bd6f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7c3189-8b84-401b-8c7c-480e630619a1",
                    "LayerId": "204afa92-972f-4625-821f-7e1fc8f10bbf"
                },
                {
                    "id": "9b2437e1-6392-4f71-a122-2114a33c5f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7c3189-8b84-401b-8c7c-480e630619a1",
                    "LayerId": "921d0313-c4dc-4ea6-8666-07fa2d91d0a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "921d0313-c4dc-4ea6-8666-07fa2d91d0a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6e84007-1c84-4fdd-a6ea-989ba0014049",
            "blendMode": 1,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "204afa92-972f-4625-821f-7e1fc8f10bbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6e84007-1c84-4fdd-a6ea-989ba0014049",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}