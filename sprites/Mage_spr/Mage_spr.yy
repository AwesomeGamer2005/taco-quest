{
    "id": "70f187a7-3126-4060-9324-52dcf94db69a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mage_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 5,
    "bbox_right": 60,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44f7243f-c559-43b3-988a-229e642c0943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70f187a7-3126-4060-9324-52dcf94db69a",
            "compositeImage": {
                "id": "a85ed3a3-7cd8-4818-a62f-cfc184ecc5e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44f7243f-c559-43b3-988a-229e642c0943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797c1231-61c0-49c6-b081-13bbdad1f4fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44f7243f-c559-43b3-988a-229e642c0943",
                    "LayerId": "9181915f-e55c-4730-9a28-035120f34ac0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9181915f-e55c-4730-9a28-035120f34ac0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70f187a7-3126-4060-9324-52dcf94db69a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}