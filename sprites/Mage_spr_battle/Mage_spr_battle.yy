{
    "id": "d5792cd2-a27b-4121-890f-d8c565be261c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mage_spr_battle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 5,
    "bbox_right": 60,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe228c3c-6827-42e9-8a57-f37bf120b710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5792cd2-a27b-4121-890f-d8c565be261c",
            "compositeImage": {
                "id": "b6fefcaa-6517-4318-a60d-5c688f4d097c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe228c3c-6827-42e9-8a57-f37bf120b710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b02795b6-287e-411c-aae3-7ce184fba94a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe228c3c-6827-42e9-8a57-f37bf120b710",
                    "LayerId": "438ec900-6943-4c15-af98-08727d08a550"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "438ec900-6943-4c15-af98-08727d08a550",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5792cd2-a27b-4121-890f-d8c565be261c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}