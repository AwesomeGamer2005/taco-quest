{
    "id": "3be4246e-a427-4c86-82b7-eec86fe931d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mage_spr_battle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 3,
    "bbox_right": 58,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47185383-89f3-4c21-8754-6869518965d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be4246e-a427-4c86-82b7-eec86fe931d8",
            "compositeImage": {
                "id": "248089c5-2159-44dd-b8eb-7c724c8e3c63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47185383-89f3-4c21-8754-6869518965d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f603fc8e-54a3-4d60-b9c0-16e03ad1a324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47185383-89f3-4c21-8754-6869518965d6",
                    "LayerId": "2558d8b8-3940-4a50-9f44-4f3fd0280018"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2558d8b8-3940-4a50-9f44-4f3fd0280018",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3be4246e-a427-4c86-82b7-eec86fe931d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}