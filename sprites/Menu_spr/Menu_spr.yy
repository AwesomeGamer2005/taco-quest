{
    "id": "cbe32fd5-5da5-4f44-a385-95ce31b28133",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Menu_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 722,
    "bbox_left": 369,
    "bbox_right": 879,
    "bbox_top": 348,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f3c3acf-1bf3-4601-a7c4-a1c58eddb464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe32fd5-5da5-4f44-a385-95ce31b28133",
            "compositeImage": {
                "id": "7d39ca98-a41c-4d63-9f1c-90ada88562de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3c3acf-1bf3-4601-a7c4-a1c58eddb464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c00f5971-b9b0-4f6b-bf82-425de15cf5ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3c3acf-1bf3-4601-a7c4-a1c58eddb464",
                    "LayerId": "8917fcc8-9e48-4eb9-8605-26c907b90cec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "8917fcc8-9e48-4eb9-8605-26c907b90cec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe32fd5-5da5-4f44-a385-95ce31b28133",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}