{
    "id": "34ff8f59-25b0-45fd-b330-c135d048e39d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Pirate_locked_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 3,
    "bbox_right": 63,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cc03945-8bd5-45ba-b818-1600d9eaee47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34ff8f59-25b0-45fd-b330-c135d048e39d",
            "compositeImage": {
                "id": "57967c6a-a761-4b23-ba23-d49e1c295747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc03945-8bd5-45ba-b818-1600d9eaee47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f48c97b-bc4a-4c66-a900-ba0d46d0ad40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc03945-8bd5-45ba-b818-1600d9eaee47",
                    "LayerId": "e4c33b3e-cb5f-4d8e-a838-e37755359f0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e4c33b3e-cb5f-4d8e-a838-e37755359f0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34ff8f59-25b0-45fd-b330-c135d048e39d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}