{
    "id": "2c2e3700-925f-4974-84ac-51cdc8124afb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Pirate_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 3,
    "bbox_right": 63,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59baa610-e7f5-4d0a-861e-90e79a98a02b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c2e3700-925f-4974-84ac-51cdc8124afb",
            "compositeImage": {
                "id": "177c9e53-bebb-4b43-9839-55b153ea5463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59baa610-e7f5-4d0a-861e-90e79a98a02b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda4d4eb-ed0f-40c7-95f7-2a1c778aecc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59baa610-e7f5-4d0a-861e-90e79a98a02b",
                    "LayerId": "eaef71e1-ef34-4da8-b5e8-20ccad7cbe23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eaef71e1-ef34-4da8-b5e8-20ccad7cbe23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c2e3700-925f-4974-84ac-51cdc8124afb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}