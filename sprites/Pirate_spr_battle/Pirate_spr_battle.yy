{
    "id": "e04c3b73-7540-4094-a06a-2b954cbc766d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Pirate_spr_battle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 3,
    "bbox_right": 63,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6233314-b167-4d34-92aa-944b2d0dd96a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e04c3b73-7540-4094-a06a-2b954cbc766d",
            "compositeImage": {
                "id": "c27659c5-0c1f-4cf2-9f6c-234e593fa663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6233314-b167-4d34-92aa-944b2d0dd96a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce9b4cd6-22f3-4d15-897b-74212d87078e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6233314-b167-4d34-92aa-944b2d0dd96a",
                    "LayerId": "17d28542-6ed1-4c0f-bfe9-0f3b24af798c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "17d28542-6ed1-4c0f-bfe9-0f3b24af798c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e04c3b73-7540-4094-a06a-2b954cbc766d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}