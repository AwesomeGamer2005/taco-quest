{
    "id": "c8076e57-29f6-4039-884f-47c579e0a712",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Pirate_spr_battle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e54c2659-d363-469f-8aff-3f687d537c93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8076e57-29f6-4039-884f-47c579e0a712",
            "compositeImage": {
                "id": "c2dea3ec-d64e-4258-acc3-5fea9ab3cc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e54c2659-d363-469f-8aff-3f687d537c93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41dbde9-d95f-4e25-adc1-75c69a42f4aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e54c2659-d363-469f-8aff-3f687d537c93",
                    "LayerId": "87eb9524-3f8c-4ac1-be98-60e593577483"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "87eb9524-3f8c-4ac1-be98-60e593577483",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8076e57-29f6-4039-884f-47c579e0a712",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}