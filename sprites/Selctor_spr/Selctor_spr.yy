{
    "id": "8510e358-e88b-4b30-87d5-3b9ab02c9345",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Selctor_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52e24c42-c087-4a19-b1fc-bf820277f007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8510e358-e88b-4b30-87d5-3b9ab02c9345",
            "compositeImage": {
                "id": "ea4d5bff-1883-49c3-bc4a-b73b9a7b5ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e24c42-c087-4a19-b1fc-bf820277f007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b622b117-2486-4874-8dc6-af36a7491204",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e24c42-c087-4a19-b1fc-bf820277f007",
                    "LayerId": "a337ec5c-5e00-4da9-b4a5-1d5a6c6a5ac2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a337ec5c-5e00-4da9-b4a5-1d5a6c6a5ac2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8510e358-e88b-4b30-87d5-3b9ab02c9345",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}