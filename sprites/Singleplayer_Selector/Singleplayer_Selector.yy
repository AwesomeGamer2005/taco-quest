{
    "id": "fba3c26a-2329-47ec-8934-97c9455e78fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Singleplayer_Selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2b55faa-5529-4d8d-a23a-33dc3dc1ec0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fba3c26a-2329-47ec-8934-97c9455e78fd",
            "compositeImage": {
                "id": "a49e4d06-92bd-40f5-bf6b-d90eae8cc4c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2b55faa-5529-4d8d-a23a-33dc3dc1ec0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc8e835-88fe-4e2f-ae51-06f20df782c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2b55faa-5529-4d8d-a23a-33dc3dc1ec0e",
                    "LayerId": "bb15aa5f-70f0-47c9-bf99-03a8ea1e78b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "bb15aa5f-70f0-47c9-bf99-03a8ea1e78b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fba3c26a-2329-47ec-8934-97c9455e78fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}