{
    "id": "88b546fb-9654-4dca-9066-c594f5abed42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Title_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 372,
    "bbox_left": 176,
    "bbox_right": 861,
    "bbox_top": 168,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f516f4a1-da03-4b22-849f-4e2ea5636e60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88b546fb-9654-4dca-9066-c594f5abed42",
            "compositeImage": {
                "id": "4c1feeac-62c3-4f2b-bf3e-63f095c36ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f516f4a1-da03-4b22-849f-4e2ea5636e60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19ab71f8-1366-4b60-9b51-db8473878f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f516f4a1-da03-4b22-849f-4e2ea5636e60",
                    "LayerId": "ac2e1578-81c1-4e17-b8ee-4ae1a35ecbc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "ac2e1578-81c1-4e17-b8ee-4ae1a35ecbc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88b546fb-9654-4dca-9066-c594f5abed42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}