{
    "id": "61854737-b5f6-45ca-8a81-31d9cc178354",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "UpRightFloor_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d092cb74-2f56-4bc2-8f08-752d7312874f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61854737-b5f6-45ca-8a81-31d9cc178354",
            "compositeImage": {
                "id": "1dbce90f-5e02-456d-8fd7-d2a9fedc3308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d092cb74-2f56-4bc2-8f08-752d7312874f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8349b417-9eb4-4086-b4bd-f37d07cd1fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d092cb74-2f56-4bc2-8f08-752d7312874f",
                    "LayerId": "21d0e692-34d5-405c-9534-76087bc5e25e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "21d0e692-34d5-405c-9534-76087bc5e25e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61854737-b5f6-45ca-8a81-31d9cc178354",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}