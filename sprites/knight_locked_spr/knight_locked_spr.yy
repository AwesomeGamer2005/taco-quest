{
    "id": "e25b47fd-4609-4edc-a1f4-263c4851d7aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "knight_locked_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "174ef8da-657b-4fe9-af50-efa434f894bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e25b47fd-4609-4edc-a1f4-263c4851d7aa",
            "compositeImage": {
                "id": "82baef27-911f-426b-a64d-725db3a75baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "174ef8da-657b-4fe9-af50-efa434f894bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e7717f4-8d98-4f85-9fc4-f4acd596e6bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174ef8da-657b-4fe9-af50-efa434f894bd",
                    "LayerId": "78e95c74-d959-4ea7-abbd-099d42696b9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "78e95c74-d959-4ea7-abbd-099d42696b9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e25b47fd-4609-4edc-a1f4-263c4851d7aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}