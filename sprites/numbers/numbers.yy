{
    "id": "73c0c606-2740-4381-8ff2-46d07e31ce41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "numbers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 603,
    "bbox_left": 333,
    "bbox_right": 694,
    "bbox_top": 221,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a61129a0-2455-4b48-a1fa-dd2a60d152b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73c0c606-2740-4381-8ff2-46d07e31ce41",
            "compositeImage": {
                "id": "be46be41-a598-4b0b-ba61-96cd2831f977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a61129a0-2455-4b48-a1fa-dd2a60d152b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b92fadc2-2a01-4723-b758-f11da471273b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a61129a0-2455-4b48-a1fa-dd2a60d152b7",
                    "LayerId": "8b63558a-a04c-45d8-84bb-d5cda8e08879"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "8b63558a-a04c-45d8-84bb-d5cda8e08879",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73c0c606-2740-4381-8ff2-46d07e31ce41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}