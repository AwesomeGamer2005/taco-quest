{
    "id": "95cac8bc-f7f2-4dff-af45-d982fd7b6d7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "selector2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92178374-13d5-4d04-9ba7-90f3cdc35451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95cac8bc-f7f2-4dff-af45-d982fd7b6d7c",
            "compositeImage": {
                "id": "ed5cb775-8a78-4a22-82d3-8b19f1e84ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92178374-13d5-4d04-9ba7-90f3cdc35451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6bbbd9b-fc4b-46bf-8c06-c14aa94fd9ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92178374-13d5-4d04-9ba7-90f3cdc35451",
                    "LayerId": "02bb05ff-f324-4445-82b7-e5ee287bf2ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "02bb05ff-f324-4445-82b7-e5ee287bf2ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95cac8bc-f7f2-4dff-af45-d982fd7b6d7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}