{
    "id": "08783562-bb6f-4b5d-a6b3-3a0a483c93df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Battle_Menu_Part1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 749,
    "bbox_left": 16,
    "bbox_right": 1007,
    "bbox_top": 463,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6139e779-a6c0-4ca2-ac90-4a65e48aa1f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08783562-bb6f-4b5d-a6b3-3a0a483c93df",
            "compositeImage": {
                "id": "91960dc3-99d7-41b2-9f6a-011bb0ce8c71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6139e779-a6c0-4ca2-ac90-4a65e48aa1f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ebe4428-cbeb-4347-926f-84b70c04c5b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6139e779-a6c0-4ca2-ac90-4a65e48aa1f2",
                    "LayerId": "d5e397e8-e582-4e9c-88f9-863f7dc16b4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "d5e397e8-e582-4e9c-88f9-863f7dc16b4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08783562-bb6f-4b5d-a6b3-3a0a483c93df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}