{
    "id": "4e48141e-c5e8-4a17-a30b-716760735e8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Battle_selcore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 287,
    "bbox_left": 0,
    "bbox_right": 424,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af93a270-97d8-4d42-9a76-0140e1aabf07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e48141e-c5e8-4a17-a30b-716760735e8e",
            "compositeImage": {
                "id": "4023ed31-19e1-4147-88b9-9db134c948eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af93a270-97d8-4d42-9a76-0140e1aabf07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63192333-bfcb-4e82-b6a9-9450d4df0b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af93a270-97d8-4d42-9a76-0140e1aabf07",
                    "LayerId": "cd2435c4-0044-40de-950f-2087728797e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "cd2435c4-0044-40de-950f-2087728797e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e48141e-c5e8-4a17-a30b-716760735e8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 0,
    "yorig": 0
}